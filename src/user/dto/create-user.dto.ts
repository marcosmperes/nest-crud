import { IsString, IsEmail, MinLength, IsOptional, IsDateString } from "class-validator";

export class CreateUserDTO {

    @IsString()
    name:string;

    @IsEmail()
    email:string;

    @IsString()
    @MinLength(6)
    password:string;

    @IsOptional()
    @IsDateString()
    birth_at: string; 

}