import { Injectable, NotFoundException } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { CreateUserDTO } from "./dto/create-user.dto";
import { UpdatePatchUserDTO } from "./dto/update-patch-user.dto";
import { UpdatePutUserDTO } from "./dto/update-put-user.dto";

@Injectable()
export class UserService{

    constructor(private readonly prisma: PrismaService) {}

    async create({email, name, password}: CreateUserDTO) {

        return this.prisma.user.create({
            data: {
                email,
                name,
                password
            }
        })

    }

    async list() {
        return this.prisma.user.findMany();
    }

    async show(id: number) {
        return this.prisma.user.findUnique({
            where: {
                id
            }
        })
    }

    async update(id: number, {email, name, password, birth_at}: UpdatePutUserDTO) {

        return this.prisma.user.update({
            data: {email, name, password, birth_at: birth_at ? new Date(birth_at) : null},
            where: {
                id
            }
        })
    }

    async updatePartial(id: number, {email, name, password, birth_at}: UpdatePatchUserDTO) {
    
        const data: any = {};

        if(birth_at){
            data.birth_at = new Date(birth_at);
        }

        if(email){
            data.email = new Date(email);
        }

        if(name){
            data.name = new Date(name);
        }

        if(password){
            data.password = new Date(password);
        }


        return this.prisma.user.update({
            data,
            where: {
                id
            }
        })
    }

    async delete(id: number){

        if(!(await this.show(id))){
            throw new NotFoundException('Usuário não existe!');
        }

        return this.prisma.user.delete({
            where: {
                id
            }
        });
    }
}